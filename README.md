# SOR

Sistema Online de RPG

## Rodando o software

Para rodar o sistema você deverá ter todas as dependências instaladas e depois rodar os comandos descritos abaixo.

### Dependências

Sua máquina deverá ter instalado:

* nodejs
* mongodb (para o desenvolvimento e teste local) - com um banco de dados chamado "Test" com usuário test e senha test

### Comandos

```
npm install
npm run start-dev

```
