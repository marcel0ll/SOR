/**
 * File that defines what a blog post is
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoomUserSchema = new Schema({
  type: String,
  permission: String,
  logged: Boolean,
  user: {type: mongoose.Schema.Types.ObjectId, red:'User'}
})

module.exports = mongoose.model('RoomUserSchema', RoomUserSchema);
