var express = require('express');
var router = new express.Router();
var config = require('../../../config');

var Comment = require('./comment_schema');
var Post = require('../post/post_schema');
var User = require('../../user/user_schema');

router.get('/blog/posts/:perma_link/comments', getComments);
router.post('/blog/posts/:perma_link/comments', createComment);
router.put('/blog/posts/:perma_link/comments/:id', updateComment);
router.delete('/blog/posts/:perma_link/comments/:id', deleteComment);

function getComments (req, res) {
  var perma_link = req.params.perma_link;

  Post.findOne({perma_link})
    .populate('comments')
    .exec(function(err, post) {
      if (err) {
        res.send(err);
      }
      //responds with a json object of our database comments.
      res.json(post.comments)
  });
}

function createComment (req, res) {
  var perma_link = req.params.perma_link;

  Post.findOne({perma_link})
    .populate('comments')
    .exec(function(err, post) {
      if (err) {
        res.send(err);
      } else {
        //responds with a json object of our database comments.
        var comment = new Comment();
        comment.text = req.body.text;
        User.findOne({username: req.body.username})
        .exec(function(err, user) {
          comment.author = user;
          comment.post = post;
          comment.save(function(err) {
            if (err) {
              res.send({success: false, message: "Comment not created."});
            } else {
              post.comments.push(comment);
              post.save(function(err) {
                if(err) {
                  res.send(err);
                }

                res.send({success: true, message: "Comment created successfully."});
              });
            }
          });
        });
      }
  });
}

function updateComment (req, res) {

}

function deleteComment (req, res) {
  var perma_link = req.params.perma_link;

  Post.findOne({perma_link})
    .populate('comments')
    .exec(function(err, post) {
      if (err) {
        res.send(err);
      } else {
        //responds with a json object of our database comments.
        Comment.findOne({_id: req.params.id})
        .exec(function(err, comment) {
          comment.remove(function(err) {
            if (err) {
              res.send(err);
            }

            res.json({ success: true, message: 'Comment successfully removed!' });
          });
        });
      }
  });
}

module.exports = router;
