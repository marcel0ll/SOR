var express = require('express');
var marked = require('marked');
var router = new express.Router();
var config = require('../../../config');

var Post = require('./post_schema');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  smartypants: false
});


router.get('/blog/posts', getPosts);

// Admin only routes
router.post('/blog/posts', createPost);
router.put('/blog/posts/:perma_link', updatePost)
router.delete('/blog/posts/:perma_link', deletePost)

function getPosts(req, res) {
  Post.find({})
  .populate({
    path: 'comments',
    // Get friends of friends - populate the 'friends' array for every friend
    populate: { path: 'author', select: 'username'},
    options: {
      sort: {created_at: 'desc'}
    }
  })
  .sort({created_at: 'desc'})
  .exec(function(err, posts) {
    if (err) {
      res.send(err);
    }
    //responds with a json object of our database comments.
    res.json(posts)
  });
}

function createPost(req, res) {
  var post = new Post();
  post.author = req.body.author;
  post.title = req.body.title;

  var markdown = req.body.markdown || '';

  post.markdown = markdown;
  post.html = marked(markdown);
  var dateObj = new Date();
  var month = dateObj.getUTCMonth() + 1; //months from 1-12
  var day = dateObj.getUTCDate();
  var year = dateObj.getUTCFullYear();
  post.perma_link = `${year}_${month}_${day}_${post.title}`

  post.save(function(err) {
    if (err) {
      res.send(err);
    }

    res.json({ success: true, perma_link: post.perma_link, message: 'Post successfully added!' });
  });
}

function updatePost(req, res) {
  var perma_link = req.body.perma_link;
  Post.findOne({perma_link}, function(err, post) {
    post.author = req.body.author;
    post.title = req.body.title;

    var markdown = req.body.markdown || '';

    post.markdown = markdown;
    post.html = marked(markdown);

    post.save(function(err) {
      if (err) {
        res.send(err);
      }

      res.json({ success: true, message: 'Post successfully updated!' });
    });
  });

}

function deletePost(req, res) {
  var perma_link = req.params.perma_link;
  Post.findOne({perma_link}, function(err, post) {
      post.remove(function(err) {
        if (err) {
          res.send(err);
        }

        res.json({ success: true, message: 'Post successfully removed!' });
      });
  });
}

module.exports = router;
