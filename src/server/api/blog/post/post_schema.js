/**
 * File that defines what a blog post is
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Comment = require('../comment/comment_schema');


var PostSchema = new Schema({
  title: String,
  markdown: String,
  html: String,
  author: String,
  perma_link: String,
  likes: {type: Number, default: 0},
  comments : [{ type: Schema.Types.ObjectId, ref: 'Comment' }]
  // author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

PostSchema.pre('remove', function(next) {
    Comment.remove({post: this._id}).exec();
    next();
});

module.exports = mongoose.model('Post', PostSchema);
