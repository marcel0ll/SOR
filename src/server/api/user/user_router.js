var express = require('express');
var router = new express.Router();
var User = require('./user_schema');
var jwt = require('jsonwebtoken');
var config = require('../../config');
var bcrypt = require('bcrypt');

// User routers goes here
router.post('/user/', authenticate);
router.put('/user/', createUser);
router.post('/user/data/', getUserData);
router.patch('/user/', updateUser);

// User routes functions goes here
function authenticate(req, res) {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) {
      throw err;
    }

    if (!user) {
      res.json({success: false, message: 'Authentication failed.'});
    } else {
      bcrypt.compare(req.body.password, user.password, function(err, auth) {
        if(auth) {
          let token = jwt.sign(user, config.secret, {expiresIn: 60 * 24});
          res.json({success: true, username: user.username, email: user.email, roles: user.roles, nickname: user.nickname, preferredNarratives: user.preferredNarratives, preferredSystems: user.preferredSystems, preferredRole: user.preferredRole, realName: user.realName, phone: user.phone, socialMedia: user.socialMedia, token: token});
        } else {
          res.json({success: false, message: 'Authentication failed.'});
        }
      });
    }
  })
}

function createUser(req, res) {
  var username = req.body.username;
  var email = req.body.email;
  var password = req.body.password;
  var passcheck = req.body.passcheck;

  if (passcheck !== password) {
	  res.json({success: false, message: 'User not created: Password doesn\'t match'});
  }
  else if (!username) {
    res.json({success: false, message: 'User not created: Username can\'t be empty'});
  }
  else if (!password) {
	res.json({success: false, message: 'User not created: Password can\'t be empty'});
  }
  else if (!email){
	res.json({success: false, message: 'User not created: Email can\'t be empty'});
  }
  else {
    User.count({
      username: username
    }, function(err, count) {
      if (count <= 0) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(password, salt, function(err, hash) {
			  // Store hash in your password DB.
			  var newUser = new User({username: username, email: email, password: hash});

			  newUser.save(function(err) {
				if (err) {
				  throw err;
				}

				res.json({success: true, message: 'User created with success'});
			  })
			});
		})
	  }
	  else {
		res.json({success: false, message: 'User not created: Username already taken'});
	  }
    });
  }
}

function getUserData(req, res) {

}

function updateUser(req, res) {
	var email = req.body.email;
	//var password = req.body.password;
	var nickname = req.body.nickname;
	var preferredNarratives = req.body.preferredNarratives;
	var preferredSystems = req.body.preferredSystems;
	var preferredRole = req.body.preferredRole;
	var realName = req.body.realName;
	var phone = req.body.phone;
	var socialMedia = req.body.socialMedia;

	User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) {
      throw err;
    }

    if (!user) {
      res.json({success: false, message: 'User is invalid.'});
    } else {
      //doshithere
    }
  })
}


module.exports = router;
