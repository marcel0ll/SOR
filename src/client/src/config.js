let server = '';
let socket = '';
if (process.env.NODE_ENV === 'production') {
  server = 'sor-app-171922.appspot.com'
  socket = 'sor-app-171922.appspot.com'
} else {
  server = 'localhost:3001'
  socket = 'localhost:3001'
}


export const API = {
	server,
  socket,

	get root(){
		return 'http://'+this.server+'/api';
	},

	get user() {
		return this.root+"/user";
	},
	get userData() {
		return this.user+"/data";
	},
	get blog_posts() {
		return this.root+"/blog/posts";
	}
}
