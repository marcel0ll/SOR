import React from 'react';

export default class Datatable extends React.Component{

  constructor(props) {
    super(props);

    let columns = this.props.columns;
    let self = this;
    if( this.props.multiSelect ) {
      let selectionColumn = {
        title: (<SelectorColumn
          handleClick={()=>{

          if ( self.props.selected.length > 0 ) {
            self.props.handleSelect([]);
          } else {
            let arr = [];
            for(let i = 0; i < self.props.data.length; i++) {
              arr.push(i);
            }
            self.props.handleSelect(arr);
          }
        }}
        checked={()=>{
          // return self.props.selected.length === self.props.data.length ? true : false;
          return false;
        }}
        ></SelectorColumn>),
        key: "selected",
        component: SelectorColumn,
        props: {
          handleClick: (id) => {
            let selected = self.props.selected;
            if (selected.includes(id)) {
              selected.splice(selected.indexOf(id), 1);
            } else {
              selected.push(id);
            }
            self.props.handleSelect(selected);
          },
          checked: (id) => {
            return self.props.selected.includes(id);
          }
        },
        size: "40px"
      }
      columns = [selectionColumn, ...this.props.columns];
    }

    this.state = {
      columns: columns
    }

  }

  render () {
    return (
      <div>
        <nav className="level">
          <div className="level-left">
            <LeftController selected={this.props.selected} handleCreate={this.props.handleCreate} handleView={this.props.handleView} handleEdit={this.props.handleEdit} handleDelete={this.props.handleDelete}/>
          </div>

          <div className="level-right">
            <RightController searchPlaceholder={this.props.searchPlaceholder}/>
          </div>
        </nav>
        <table className="table">
        <thead>
          <tr>
            {
              this.state.columns.map( (column, index) => {
                return (
                  <th key={index} style={{width: column.size}}>
                    {column.title}
                  </th>
                )
              })
            }
          </tr>
        </thead>
        <tfoot>
          <tr>
          {
            this.state.columns.map( (column, index) => {
              return (
                <th key={index}>
                  {column.title}
                </th>
              )
            })
          }
          </tr>
        </tfoot>
        <tbody>
          {
            this.props.data.map( (row, rindex) => {
              return (
                <tr key={rindex}>
                {
                  this.state.columns.map( (column, cindex) => {
                    return (
                      <td key={cindex}>
                        <column.component {...row} {...column.props} row={rindex}/>
                      </td>
                    )
                  })
                }
                </tr>
              )
            })
          }
        </tbody>
      </table>
        <nav className="level">
          <div className="level-left">
            <div className="level-item">
              <p> {this.props.data.length} items </p>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

function LeftController(props) {
  return (
    <div className="field has-addons">
      {
        props.handleCreate ?
        <p className="control">
          <a onClick={props.handleCreate} className="button is-success">
            <span className="icon is-small">
              <i className="fa fa-plus" />
            </span>
            <span>Create</span>
          </a>
        </p>
        : null
      }
      {
        props.handleView ?
        <p className="control">
          <a onClick={props.selected.length ? props.handleView : null} className="button is-info" disabled={props.selected.length <= 0 ? "disabled" : false}>
            <span className="icon is-small">
              <i className="fa fa-eye" />
            </span>
            <span>View</span>
          </a>
        </p>
        : null
      }
      {
        props.handleEdit ?
        <p className="control">
          <a onClick={props.selected.length ? props.handleEdit : null} className="button is-warning" disabled={props.selected.length <= 0 ? "disabled" : false}>
            <span className="icon is-small">
              <i className="fa fa-pencil-square-o" />
            </span>
            <span>Edit</span>
          </a>
        </p>
        : null
      }
      {
        props.handleDelete ?
        <p className="control">
          <a onClick={props.selected.length ? props.handleDelete : null} className="button is-danger" disabled={props.selected.length <= 0 ? "disabled" : false}>
            <span className="icon is-small">
              <i className="fa fa-trash-o" />
            </span>
            <span>Delete</span>
          </a>
        </p>
        : null
      }
    </div>
  )
}

function RightController(props) {
  return (
    <div className="level-item">
      <div className="field has-addons">
        <p className="control">
          <input className="input" type="text" placeholder={props.searchPlaceholder ? props.searchPlaceholder : "Search"} />
        </p>
        <p className="control">
          <button className="button">
            Search
          </button>
        </p>
      </div>
    </div>
  )
}

function SelectorColumn(props) {
  return (
    <input onClick={props.handleClick.bind(null, props.row)} type="checkbox" checked={props.checked(props.row) ? 'checked' : false} />
  );
}
