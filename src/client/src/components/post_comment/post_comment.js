import React from 'react';
import './post_comment.css';

export default function PostComment (props) {
  if (!props._comment ) return null;
  return (
    <div className="comment level box">
      <div className="level-left">
        <div className="level-item">
          {props._comment.author.username +': '}{props._comment.text}
        </div>
      </div>
    </div>
  );
}
