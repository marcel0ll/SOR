import React from 'react';
import ConditionalNavLink from '../conditional_nav_link/conditional_nav_link'
import './nav_bar.css';

import Auth from '../../services/auth';

export default class NavBar extends React.Component{

  constructor(props) {
    super(props);

    this.state = {
      menuToogled: false
    }

    this.handleLogout = this.handleLogout.bind(this);
    this.toogleMobileMenu = this.toogleMobileMenu.bind(this);
  }

  toogleMobileMenu(event) {
    this.setState({menuToogled: !this.state.menuToogled});
  }

  handleLogout() {
    Auth.logout();
    this.setState({});
  }

  render () {
    return (
      <div className="nav_bar">
        <nav className="nav">
          <div className="nav-left">
            <a className="nav-item">
              <h1> SOR </h1>
            </a>
          </div>
          {/* MOBILE ONLY */}
          <span className="nav-toggle" onClick={this.toogleMobileMenu}>
            <span></span>
            <span></span>
            <span></span>
          </span>
          {/* NON MOBILE ONLY */}
          <div className={"nav-right nav-menu" + (this.state.menuToogled ? ' is-active' : '')}>
            <ConditionalNavLink condition={!Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" exact to="/">Blog</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" exact to="/">Home</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/blog">Blog</ConditionalNavLink>
            <ConditionalNavLink condition={!Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/login">Login</ConditionalNavLink>
			      <ConditionalNavLink condition={!Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/register">Register</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.is('admin')} className="nav-item is-tab" activeClassName="is-active" to="/cms">CMS</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/friends">Friends</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/tables">Tables</ConditionalNavLink>
            <ConditionalNavLink condition={Auth.isAuthenticated} className="nav-item is-tab" activeClassName="is-active" to="/profile">Profile</ConditionalNavLink>
            { Auth.isAuthenticated ? <div className="nav-item"> <button className="button is-primary" onClick={this.handleLogout}>Logout</button> </div> : null }
          </div>
        </nav>
      </div>
    )
  }
}
