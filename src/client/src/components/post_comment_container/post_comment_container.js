import React from 'react';
import './post_comment_container.css';

export default function PostCommentContainer (props) {
  return (
    <div className="comment_container">
      {props.children}
    </div>
  );
}
