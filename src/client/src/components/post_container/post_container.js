import React from 'react';

export default function PostContainer (props) {
  return (
    <div className='columns'>
      <div className='column is-6 is-offset-3'>
        {props.children}
      </div>
    </div>
  );
}
