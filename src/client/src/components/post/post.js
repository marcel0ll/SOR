import React from 'react';
import moment from 'moment';
import {
  Link,
} from 'react-router-dom'

import './post.css';

export default function Post (props) {
  if (!props.post ) return null;
  return (
    <div className="box post">
      <div className="level">
        <div className="level-left">
          <Link to={"/blog/" + props.post.perma_link }> <h3 className="title"> {props.post.title} </h3> </Link>
        </div>
        <div className="level-right">
          <div className="subtitle"> {moment(props.post.created_at).format('DD/MM/YYYY - HH:mm')} </div>
        </div>
      </div>
      <div className='post-separator'></div>
      <div className='post-content content' dangerouslySetInnerHTML={{__html: props.post.html}}></div>
      <div className='post-separator'></div>
      <div className="level">
        <div className="level-left">
          <a className="level-item">
            <span className="likes tag is-black">
              <span className="icon is-small"><i className="fa fa-heart"></i></span>
              {props.post.likes}
            </span>
          </a>
          <a className="level-item">
            <span className="likes tag is-black">
              <span className="icon is-small"><i className="fa fa-comments"></i></span>
              {(props.post.comments && props.post.comments.length) || 0}
            </span>
          </a>
        </div>
        <div className="level-right">
          <span>{props.post.author}</span>
        </div>
      </div>
    </div>
  );
}
