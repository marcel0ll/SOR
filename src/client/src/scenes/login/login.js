import React from 'react';
import Auth from '../../services/auth';
import './login.css';

export default class Login extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: ''
    }

    this.handleLogin = this.handleLogin.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleLogin(event) {
    event.preventDefault();
    Auth.login(this.state.user, this.state.password, () => {
      this.props.history.push('/')
    });
  }

  handleUserChange(event) {
    this.setState({user: event.target.value});
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  render () {
    return (
      <div className="columns">
        <div className="column is-2 is-offset-5 has-text-centered">
          <div className="login">
            <h2 className="title"> Login </h2>
            <form onSubmit={this.handleLogin} className="has-text-left">
              <div className="field">
                <label className="label">Username</label>
                <p className="control">
                  <input className="input" type="text" name="user" placeholder="Username" value={this.state.user} onChange={this.handleUserChange} />
                </p>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <p className="control">
                  <input className="input" type="password" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
                </p>
              </div>
              <div className="field">
                <p className="control">
                  <input className="button is-primary" type="submit" value="Submit" />
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
