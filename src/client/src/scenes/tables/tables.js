import React from 'react';

import TablesDataTable from './components/datatable/datatable';
import TableService from 'services/tables';

import Table from './scenes/table/table';

import {
  Route,
} from 'react-router-dom'

export default class Tables extends React.Component{

  render () {
    return (
      <div>
        <Route exact path={`${this.props.match.url}/`} render={props =>
          <TablesDataTable data={TableService.tables} {...props}/>
        }/>
        <Route path={`${this.props.match.url}/table`} component={Table}/>
      </div>
    )
  }
}
