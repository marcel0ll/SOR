import React from 'react';
import Datatable from 'components/datatable/datatable';

export default class TablesDataTable extends React.Component {

  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
    this.handleView = this.handleView.bind(this);

    this.state = {
      selected: []
    }
  }

  handleSelect(selected) {
    this.setState({selected: selected});
  }

  handleView(event) {
    this.props.history.push( this.props.location.pathname + '/table/view/' + this.props.data[this.state.selected[0]].id);
  }

  render() {
    return (
      <Datatable
        columns={[
          {
            title: "Title",
            key: "name",
            component: Title,
            props: {
              handleClick: () => {console.log("Halo");}
            }
          },
          {
            title: "Description",
            key: "description",
            component: Description,
            props: {
              handleClick: () => {console.log("Halo");}
            }
          }
        ]}

        searchPlaceholder="Find a table"
        multiSelect={true}
        data={this.props.data}
        selected={this.state.selected}

        handleView={this.handleView}
        handleSelect={this.handleSelect}
      />
    )
  }

}

function Title(props) {
  return (<h1>{props.name}</h1>);
}

function Description(props) {
  return (<h1>{props.description}</h1>);
}
