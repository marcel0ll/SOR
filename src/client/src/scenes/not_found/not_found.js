import React from 'react';

export default class NotFound extends React.Component{

  render () {
    return (
      <h2> Sorry, but this page is in another castle! </h2>
    )
  }
}
