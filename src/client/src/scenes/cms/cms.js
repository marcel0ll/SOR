import React from 'react';
import {
  Route,
  Link
} from 'react-router-dom'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Blog from './tabs/blog/blog';

export default class Cms extends React.Component {

  render() {
    return (
      <div>
        <Tabs selectedTabClassName="is-active">
          <div className="tabs is-boxed is-centered">
            <TabList>
              <Tab>
                <Link to={`${this.props.match.url}/blog`}>
                  <span className="icon is-small">
                    <i className="fa fa-newspaper-o"></i>
                  </span>
                  <span>Blog</span>
                </Link>
              </Tab>
              <Tab className="alpha">
                <Link to={`${this.props.match.url}/users`}>
                  <span className="icon is-small">
                    <i className="fa fa-users"></i>
                  </span>
                  <span>Users</span>
                </Link>
              </Tab>
            </TabList>
          </div>

          <TabPanel>
            <Route path={`${this.props.match.url}/blog`} render={props =>
              <Blog {...props} />
            }/>
          </TabPanel>
          <TabPanel>
            <Route path={`${this.props.match.url}/users`} render={props =>
              <h2>Any content 2</h2>
            }/>
          </TabPanel>
        </Tabs>


      </div>
    )
  }
}
