import React from 'react';
import AceEditor from 'react-ace';

import 'brace/mode/markdown';
import 'brace/theme/terminal';

export default class CuPostForm extends React.Component {

  constructor(props) {
    super(props);

    let post = props.post || {
      author: '',
      title: '',
      markdown: ''
    }

    this.state = post;

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAuthorChange = this.handleAuthorChange.bind(this);
    this.handleMarkdownChange = this.handleMarkdownChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.handleSubmit(this.state);
  }

  handleAuthorChange(event) {
    this.setState({author: event.target.value});
  }

  handleTitleChange(event) {
    this.setState({title: event.target.value});
  }

  handleMarkdownChange(value) {
    this.setState({markdown: value});
  }

  render () {
    return (
      <form onSubmit={this.handleSubmit} className="has-text-left">
        <div className="field">
          <label className="label">Author</label>
          <p className="control">
            <input className="input" type="text" name="author" placeholder="Author" value={this.state.author} onChange={this.handleAuthorChange}/>
          </p>
        </div>
        <div className="field">
          <label className="label">Title</label>
          <p className="control">
            <input className="input" type="text" name="title" placeholder="Title" value={this.state.title} onChange={this.handleTitleChange}/>
          </p>
        </div>
        <div className="field">
          <label className="label">Markdown</label>
          <AceEditor
            mode="markdown"
            theme="terminal"
            onChange={this.handleMarkdownChange}
            name="markdown"
            wrapEnabled={true}
            editorProps={{$blockScrolling: true}}
            value={this.state.markdown}
            width="100%"
            fontSize="16px"
          />
        </div>
        <div className="field">
          <p className="control">
            <input className="button is-primary" type="submit" value={this.props.text}/>
          </p>
        </div>
      </form>
    )
  }
}
