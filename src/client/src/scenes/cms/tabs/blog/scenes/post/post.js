import React from 'react';
import Posts from 'services/posts';

import PostContainer from 'components/post_container/post_container';
import PostComponent from 'components/post/post';

import PostCommentContainer from 'components/post_comment_container/post_comment_container';
import PostComment from 'components/post_comment/post_comment';

import CuPostForm from './components/cu_post_form/cu_post_form';
import CommentsDataTable from './components/comments_datatable/comments_datatable';
import {
  Route,
  Redirect
} from 'react-router-dom'

export default class Post extends React.Component{

  handleCreatePost(post) {
    Posts.createPost(post, function(data) {
      this.history.replace( '/cms/blog' );
    }, this);
  }

  handleUpdatePost(post) {
    Posts.updatePost(post, function(data) {
      this.history.replace( '/cms/blog' );
    }, this);
  }

  handleDelete() {
    this.setState({});
  }

  render () {
    return (
      <div>
        <Route exact path={`${this.props.match.url}/new`} render={props =>
          (
          <div className="columns">
            <div className="column is-4 is-offset-4">
              <CuPostForm  text="Create" handleSubmit={this.handleCreatePost} {...props} />
            </div>
          </div>
          )
        }/>
        <Route exact path={`${this.props.match.url}/edit/:perma_link`} render={props => {
          let post = Posts.getPost(props.match.params.perma_link);
          if(post) {
            return (
              <div className="columns">
                <div className="column is-4 is-offset-1">
                  <p className="title"> Edit Post: </p>
                  <CuPostForm text="Update" post={post} handleSubmit={this.handleUpdatePost} {...props} />
                </div>
                <div className="column is-6">
                  <p className="title"> Edit comments: </p>
                  <CommentsDataTable data={post} handleDelete={this.handleDelete.bind(this)} {...props} />
                </div>
              </div>
            );
          } else {
            return (
              <Redirect to="/cms/blog"/>
            )
          }
        }}/>
        <Route path={`${this.props.match.url}/view/:perma_link`} render={props => {
          let post = Posts.getPost(props.match.params.perma_link);
          if(post) {
            return (
              <PostContainer>
                <PostComponent post={post} {...props} />
                Comments:
                <PostCommentContainer>
                  {
                    post.comments && post.comments.map(function(comment, index) {
                      return (<PostComment key={index} _comment={comment} />)
                    })
                  }
                </PostCommentContainer>
              </PostContainer>
            )
          } else {
            return (
              <Redirect to="/cms/blog"/>
            )
          }

        }}
        />
      </div>
    )
  }
}
