import React from 'react';
import {
  Route,
} from 'react-router-dom'

import BlogTabDatatable from './components/datatable/datatable';

import Posts from 'services/posts';

import Post from './scenes/post/post';

export default class Blog extends React.Component{

  constructor(props) {
    super(props);

    this.state = {posts: Posts.posts};
  }

  componentDidMount() {
    Posts.fetch((posts) => {
      this.setState({posts: posts});
    })
  }

  render () {
    return (
      <div>
        <Route exact path={`${this.props.match.url}/`} render={props =>
          <BlogTabDatatable data={Posts.posts} {...props} />
        }/>
        <Route path={`${this.props.match.url}/post`} component={Post}/>
      </div>
    )
  }
}
