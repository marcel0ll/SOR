import axios from 'axios';
import {API} from 'config';

class Posts {

  constructor() {
    this._posts = [];
    this.fetch();
  }

  fetch(callback) {
    let posts = this;
    axios.get(API.blog_posts, {

    })
    .then(res => {
      posts._posts = res.data;
      if (callback) {
        callback(posts._posts);
      }
    })
    .catch(err => {
      console.error(err);
    });
  }

  createPost(post, callback, obj) {
    let posts = this;
    axios.post(API.blog_posts, post)
    .then(res => {
      console.log(res.data.success);
      let dateObj = new Date();
      let month = dateObj.getUTCMonth() + 1; //months from 1-12
      let day = dateObj.getUTCDate();
      let year = dateObj.getUTCFullYear();
      post.perma_link = `${year}_${month}_${day}_${post.title}`
      posts._posts.splice(0, 0, post);
      callback.call(obj, res.data);
    })
    .catch(err => {
      console.error(err);
      callback.call(obj, err);
    });
  }

  getPost(perma_link) {
    let post = this._posts.find(function(toFind){return toFind.perma_link === perma_link});
    return post;
  }

  updatePost(post, callback, obj) {
    let posts = this;
    axios.put(API.blog_posts+'/'+post.perma_link, post)
    .then(res => {
      console.log(res.data.success);
      let id_to_update = posts._posts.findIndex(function(toFind){return toFind.perma_link === post.perma_link});
      posts._posts[id_to_update] = post;
      callback.call(obj, res.data);
    })
    .catch(err => {
      console.error(err);
      callback.call(obj, err);
    });
  }

  deletePost(post, callback, obj) {
    let posts = this;
    axios.delete(API.blog_posts+'/'+post.perma_link)
    .then(res => {
      console.log(res.data.success);
      let id_to_update = posts._posts.findIndex(function(toFind){return toFind.perma_link === post.perma_link});
      posts._posts.splice(id_to_update, 1);
      callback.call(obj, res.data);
    })
    .catch(err => {
      console.error(err);
      callback.call(obj, err);
    });
  }

  deleteComment(post, comment, callback, obj) {
    let posts = this;
    axios.delete(API.blog_posts+'/'+post.perma_link+'/comments/'+ comment._id)
    .then(res => {
      console.log(res.data.success);
      let post_id = posts._posts.findIndex(function(toFind){return toFind.perma_link === post.perma_link});
      let id_to_update = posts._posts[post_id].comments.findIndex(function(toFind){return toFind._id === comment._id});
      posts._posts[post_id].comments.splice(id_to_update, 1);
      callback.call(obj, res.data);
    })
    .catch(err => {
      console.error(err);
      callback.call(obj, err);
    });
  }

  createComment(comment, callback, obj) {
    axios.post(API.blog_posts+'/'+comment.perma_link+'/comments', comment)
    .then(res => {
      console.log(res.data.success);
      callback.call(obj, res.data);
    })
    .catch(err => {
      console.error(err);
      callback.call(obj, err);
    });
  }

  get posts() {
    return this._posts;
  }
}

export default new Posts();
