import axios from 'axios';
import {API} from 'config';

class UserData {
	constructor() {
    this.username = '';
	this.email = '';
	this.pass = '';
    this.fetch();
  }
  
  fetch(callback){
	let userData = this;
    axios.get(API.userData, {

    })
    .then(res => {
      this.username = res.data;
	  this.email = res.
      if (callback) {
        callback();
      }
    })
    .catch(err => {
      console.error(err);
    });
  }
}

export default new UserData();