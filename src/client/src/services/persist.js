export function loadUser() {
  try {
    const serializedUser = localStorage.getItem('user');
    if ( serializedUser === null ) {
      return undefined;
    }
    return JSON.parse(serializedUser);
  } catch (err) {
    return undefined
  }
}

export function saveUser(user) {
  try {
    const serializedUser = JSON.stringify(user);
    localStorage.setItem('user', serializedUser);
  } catch (err) {
    console.error("Failer to retrieve user from local");
  }
}
