import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import 'bulma/css/bulma.css';
import 'font-awesome/css/font-awesome.css';
import './index.css';
import history from 'appHist';

import {
  Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'


import NavBar from './components/nav_bar/nav_bar';
import PrivateRoute from './components/private_route/private_route';
import SplitRoute from './components/split_route/split_route';

import Blog from './scenes/blog/blog';
import Profile from './scenes/profile/profile';
import NotFound from './scenes/not_found/not_found';
import Login from './scenes/login/login';
import Register from './scenes/register/register';
import Home from './scenes/home/home';
import Friends from './scenes/friends/friends';
import Tables from './scenes/tables/tables';
import Cms from './scenes/cms/cms';

import Auth from 'services/auth';
import {loadUser} from 'services/persist';

class Main extends React.Component{

  componentWillMount() {
    let user = loadUser();
    if ( user ) {
      if( Auth.isValidToken(user.token)) {
        Auth.log(user);
      }
    }
  }

  render () {
    return (
      <Router history={history}>
        <div>
          <NavBar />

          <div className="sor_content">
            <Switch>
              <SplitRoute condition={() => Auth.isAuthenticated} exact path="/" componentTrue={Home} componentFalse={Blog}/>
              <Route path="/blog" component={Blog} />

              <PrivateRoute exact path="/friends" component={Friends} />
              <PrivateRoute path="/tables" component={Tables} />
              <PrivateRoute path="/profile" component={Profile} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Redirect exact from="/cms" to="/cms/blog"/>
              <Route path="/cms" component={Cms}/>
              <Route component={NotFound} />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

ReactDOM.render(
  <Main/>,
  document.getElementById('root')
);
